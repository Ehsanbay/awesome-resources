### awesome-resources
各种编程语言好资源收集地-持续更新,欢迎大家贡献代码!

- PHP
    * <a href="https://github.com/qieangel2013/EsParser" target="_blank">php的操作类库，通过写sql来转化dsl来查询elasticsearch</a>
    * <a href="https://github.com/chinese-poetry/chinese-poetry" target="_blank">最全中华古诗词数据库</a>
    * <a href="https://github.com/inhere/php-console" target="_blank">php-console</a>

- Laravel
    * <a href="https://github.com/overtrue/laravel-pinyin" target="_blank">Chinese to Pinyin translator for Laravel 5 / Lumen</a>
    * <a href="https://github.com/overtrue/laravel-socialite" target="_blank">laravel-socialite</a>

- Nodejs
    * <a href="https://github.com/Binaryify/NeteaseCloudMusicApi" target="_blank">网易云音乐 Node.js API service</a>
    * <a href="https://github.com/Pana/nrm" target="_blank">nrm -- NPM registry manager</a>
    * <a href="https://github.com/TakWolf/CNode-Material-Design" target="_blank">CNode 社区第三方 Android 客户端，原生 App，Material Design 风格，支持夜间模式。</a>

- Vue.js
    * <a href="https://github.com/secreter/websocket_chat" target="_blank">基于vue的websocket聊天室</a>
    * <a href="https://github.com/moshanghan/vue-mo-cli" target="_blank">vue-mo-cli 不同的环境调用不同的接口，本地代理跨域，第三方资源分离打包</a>
    * <a href="https://github.com/egoist/vue-feather-icons" target="_blank">vue-feather-icons</a>
    * <a href="https://github.com/Plortinus/vue-multiple-pages" target="_blank">vue-multiple-pages</a>
    * <a href="https://github.com/davidroyer/vue2-editor" target="_blank">vue2-editor</a>
    * <a href="https://github.com/Meituan-Dianping/mpvue" target="_blank">mpvue 基于 Vue.js 的小程序开发框架，从底层支持 Vue.js 语法和构建工具体系</a>
    * <a href="https://github.com/fengyuanchen/vue-number-input" target="_blank">Number input component for Vue.js</a>
    * <a href="https://github.com/surmon-china/vue-awesome-swiper" target="_blank">vue-awesome-swiper</a>
    * <a href="https://github.com/nmxiaowei/avue" target="_blank">avue</a>
    * <a href="https://github.com/PanJiaChen/vue-element-admin" target="_blank">vue-element-admin</a>
    * <a href="https://github.com/dai-siki/vue-image-crop-upload" target="_blank">vue图片剪裁上传组件</a>
    * <a href="https://github.com/overtrue/vue-avatar-cropper" target="_blank">Vue Avatar Cropper</a>
    * <a href="https://github.com/lin-xin/vue-toast" target="_blank">A mobile toast plugin for vue2.</a>
    * <a href="https://github.com/lin-xin/notepad" target="_blank">基于vue2.0+vuex+localStorage+sass+webpack，实现一个本地存储的记事本。兼容PC端和移动端</a>
    * <a href="https://github.com/halfrost/vue-objccn" target="_blank">用 Vue.js 开发的跨三端应用</a>
    * <a href="https://github.com/declandewet/vue-meta" target="_blank">vue-meta</a>
    * <a href="https://github.com/airyland/vux" target="_blank">vux</a>
    * <a href="https://github.com/myxingke/vueWechatPlateform" target="_blank">这是我用Vue 写的一个微信第三方公众号管理平台</a>

- JavaScript
   * <a href="https://github.com/navyxie/idcard" target="_blank">idcard 校验身份证是否合法，获取身份证详细信息</a> 
   * <a href="https://github.com/gxvv/ex-baiduyunpan" target="_blank">百度云盘、百度云盘企业版解除大文件限制，批量复制链接</a> 
   * <a href="https://github.com/ogilhinn/node-abc" target="_blank">《Node.js入门教程》By 流口水流</a> 
   * <a href="https://github.com/lin-xin/calculator" target="_blank">基于 Electron + javascript 实现的桌面计算器应用</a> 

- Mini Program
    * <a href="https://github.com/chemzqm/wept" target="_blank">微信小程序 web 端实时运行工具 </a>
    * <a href="https://github.com/sqrtqiezi/mini-music-player" target="_blank">基于小程序开发的仿百度云音乐 demo</a>
    * <a href="https://meili.github.io/min/docs/minui/" target="_blank">MinUI</a>
    * <a href="https://github.com/meili/minui" target="_blank">基于规范的小程序 UI 组件库，自定义标签组件，简洁、易用、工具化</a>
    * <a href="https://github.com/cytle/wechat_web_devtools" target="_blank">wechat_web_devtools 微信开发者工具(微信小程序)linux完美支持</a>
    * <a href="https://github.com/nanwangjkl/sliding_puzzle" target="_blank">微信小程序，一个滑块拼图游戏</a>
    * <a href="https://github.com/natee/wxapp-2048" target="_blank">微信小程序2048</a>
    * <a href="https://github.com/kraaas/timer" target="_blank">番茄时钟微信小程序版</a>
    * <a href="https://github.com/youzan/zanui-weapp" target="_blank">高颜值、好用、易扩展的微信小程序 UI 库，Powered by 有赞</a>
    * <a href="https://github.com/yingye/weapp-qrcode" target="_blank">weapp.qrcode.js 在 微信小程序 中，快速生成二维码</a>
    * <a href="https://gitee.com/sansanC/wechatApp" target="_blank">微信小程序电商源码</a>
    * <a href="https://gitee.com/mirrors/wechat-app-mall" target="_blank">微信小程序商城，微信小程序微店</a>
    * <a href="https://gitee.com/dotton/lendoo-wx" target="_blank">灵动电商开源系统之微信小程序端</a>
    * <a href="https://github.com/lin-xin/wxapp-mall" target="_blank">微信小程序 商城demo</a>
- Go语言
    * <a href="https://github.com/ouqiang/gocron" target="_blank">定时任务管理系统</a>

- PHPStorm主题
    * <a href="https://github.com/daylerees/colour-schemes" target="_blank">colour-schemes</a>
    * <a href="http://www.phpstorm-themes.com/" target="_blank">http://www.phpstorm-themes.com/</a>

- 区块链
    * <a href="https://github.com/chaozh/awesome-blockchain-cn" target="_blank">收集所有区块链(BlockChain)技术开发相关资料，包括Fabric和Ethereum开发资料</a>